function returnObjById( id )
{
  if (document.getElementById)
    var returnVar = document.getElementById(id);
  else if (document.all)
    var returnVar = document.all[id];
  else if (document.layers)
    var returnVar = document.layers[id];
  return returnVar;
}

var ArrAjax = new Object();
ArrAjax.aufruf = function (address, handler, method) 
{
  var xmlHttp = null;
  // Mozilla, Opera, Safari sowie Internet Explorer 7
  if (typeof XMLHttpRequest != 'undefined') {
    xmlHttp = new XMLHttpRequest();
  }
  if (!xmlHttp) {
    // Internet Explorer 6 und älter
    try {
      xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
    } catch(e) {
      try {
        xmlHttp  = new
          ActiveXObject("Microsoft.XMLHTTP");
      } catch(e) {
        xmlHttp  = null;
      }   
    }   
  }
  if (!xmlHttp) {
    alert('No Ajax support possible');
    throw Exception("No ajax support");
    return false;
  }
  xmlHttp.open(method, address, true);
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
      handler(xmlHttp);
    }
  }
  xmlHttp.send(null);
  return xmlHttp;
}

function nc_get_code( request ) {
    return request.responseXML.getElementsByTagName('code')[0].firstChild.nodeValue;
}

function nc_get_text( request ) {
    return request.responseXML.getElementsByTagName('text')[0].firstChild.nodeValue;
}

function nc_error (request) {
  if (nc_get_code(request) != 0) {
    logger(nc_get_code(request), nc_get_text(request));
    return true;
  } else { return false; }
}

function nc_command(ip, command, handler) {
  var url = '/cgi-bin/communicate.cgi?address=' + ip  + '&text=' + command;
  var obj = ArrAjax.aufruf(url, handler, 'GET');
  obj.ip = ip;
  obj.command = command;
  return obj;
}

function ping_command(ip, handler) {
  var url = '/cgi-bin/ping.cgi?address=' + ip;
  var obj = ArrAjax.aufruf(url, handler, 'GET');
  obj.ip = ip;
  return obj;
}

function kty_command(ip, merges, handler) {
  var url = '/cgi-bin/kty-get.cgi?address=' + ip + '&merges=' + merges;
  var obj = ArrAjax.aufruf(url, handler, 'GET');
  obj.ip = ip;
  return obj;
}

function log_get_lines() {
  return returnObjById('logconsole').getElementsByTagName('div').length;
}

function log_clean() {
  var loglines = 8;
  if ( log_get_lines() > loglines ) {
    var logconsole = returnObjById('logconsole');
    var nodes = logconsole.getElementsByTagName('div');
    while (nodes.length > loglines) {
      logconsole.removeChild(nodes[loglines]);
    }
  }
}

function logger(code, text) {
  var logconsole = returnObjById('logconsole');


  var div = document.createElement("div");
  if (code == '0') {
    div.setAttribute("class", "lognotice");
  } else {
    div.setAttribute("class", "logerror");
    text = 'Fehler: ' + text;
  }
  
  var jetzt = new Date();
  var Std = jetzt.getHours();
  var Min = jetzt.getMinutes();
  var StdAusgabe = ((Std < 10) ? "0" + Std : Std);
  var MinAusgabe = ((Min < 10) ? "0" + Min : Min);
  var text = StdAusgabe + ":" + MinAusgabe + " " + text;
  
  var neuText = document.createTextNode(text);
  div.appendChild(neuText);
  logconsole.insertBefore(div, logconsole.getElementsByTagName('div')[0]);
  log_clean();
}
