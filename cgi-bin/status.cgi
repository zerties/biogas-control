#!/usr/bin/haserl
Content-Type: text/html

<? 
  config() {
    awk 'BEGIN{ a = 0 } {print a " " $0; a = a + 1;}' < ../config/status 
  }
?>

<html>
<head>
<title>Biogas Administration - Status</title>
  <link href="/style.css"  media="screen" rel="Stylesheet" type="text/css" />
  <script src="/scripts.js" type="text/javascript"></script>
  <meta name="copyright" content="(C)opyright  2007 by Christian Dietrich <stettberger (at) dokucode.de">
<script language="JavaScript"><!--

function get_status( id ) {
  var obj = returnObjById('status' + id);
  var req = ping_command(obj.getAttribute('address'), get_status_handler);
  req.switch_id = id;
}

function get_status_handler(request) {
  var obj = returnObjById('status' + request.switch_id);
  if (nc_get_code(request) == 0) {
    obj.innerHTML = 'l&auml;uft';
    obj.setAttribute("class", "statusrunning");
  } else {
    obj.innerHTML = "l&auml;ft nicht";
    obj.setAttribute("class", "statusstopped");
  }
}


window.onload = function() {
<?
# Add all refresh handlers
config | awk '{print "get_status("$1"); setInterval(\"get_status("$1")\", 30000);\
  "}'
?>
}
//--></script>
</head>

<body>
<? cat header.html ?>
<div id='content'>
<table border="1" cellspacing="0" padding="4">
  <tr style='background-color: grey;'><td>Name</td><td>Addresse</td><td>Status</td></tr>
  <? 
  config | awk '
  {print "<tr><td>" gensub("^[^ ]* [^ ]* ", "", 1, $0) "</td><td>" $2 "</td><td \
    address=\"" $2 "\" id=\"status" $1 "\">unbekannt</td></tr>" }'
  ?>
</table>
<p><strong>Hinweis:</strong> Die aktualisierung kann einige Zeit in Anspruch nehmen.</p>
</div>
</body>
</html>

