#!/usr/bin/haserl
Content-Type: text/html

<? 
  config() {
    awk 'BEGIN{ a = 0 } {print a " " $0; a = a + 1;}' < ../config/status 
  }
?>

<html>
<head>
<title>Biogas Administration - Status</title>
  <link href="/style.css"  media="screen" rel="Stylesheet" type="text/css" />
  <script src="/scripts.js" type="text/javascript"></script>
  <meta name="copyright" content="(C)opyright  2007 by Christian Dietrich <stettberger (at) dokucode.de">
<script language="JavaScript"><!--

function get_temperature( id ) {
  var obj = returnObjById('temperature' + id);
  var req = kty_command(obj.getAttribute('host'), obj.getAttribute('merges'), get_temperature_handler);
  req.temperature_id = id;
}

function get_temperature_handler(request) {
    if (nc_get_code(request) == 0) {
      var temperatures = request.responseXML.getElementsByTagName('temperature');
      for (var i = 0; i < temperatures.length; i++) {
        var input = returnObjById("value-" + request.temperature_id + "-" + i);
        input.innerHTML=temperatures[i].firstChild.nodeValue + " C";
      }
  } else {
    var obj = returnObjById('status-' + request.temperature_id);
    obj.innerHTML = "(Verbindungsproblem)";
    obj.setAttribute("class", "statusstopped");
  }
}

function start_watch( id, delay ) 
{
  setTimeout("get_temperature(" + id + "); setInterval(\"get_temperature(\\\"" + id + "\\\")\", 45000)", delay);
}


window.onload = function() { <?
x=0;
ls ../config/temperature | while read host; do
  let time=$x*300
  echo "start_watch($x, $time);"
  let x=x+1;
done
?> }
//--></script>
</head>

<body>
<? cat header.html ?>
<div id='content'>
<? 
x=0;
ls ../config/temperature | while read host; do
  merges=`cat ../config/temperature/$host | sed -n 's/^\([0-9]\+\).*/\1 /p' |tr -d "\n"`
  echo "<div id='temperature$x' host='$host' merges='$merges'><h2>`cat ../config/temperature/$host | sed  -n 1p` <span id='status-$x'></span></h2>" 

  echo '<table border="1" cellspacing="0" padding="4"><tr  style="background-color: grey;"><td>Name</td><td>Temperatur</td></tr>'
  i=0;
  cat "../config/temperature/$host" | sed 1d |while read sensor; do 
    echo "  <tr><td>`echo $sensor | sed 's/^[^ ]* //'`</td><td id='value-$x-$i'>unbekannt</td></tr>"
    let i=i+1
  done
  echo "</table>"

  echo "</div>"
  let x=x+1
done
?>
<p><strong>Hinweis:</strong> Die aktualisierung kann einige Zeit in Anspruch nehmen.</p>
</div>
</body>
</html>

