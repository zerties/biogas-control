#!/usr/bin/haserl
Content-Type: text/xml

<?xml version="1.0" encoding="iso-8859-1"?>
<?  
answer=`echo adc get | nc_udp -H "$FORM_address" -P 2701 2>&1`
rvalue=$?
echo "<response><code>$?</code>"
if [ $rvalue -eq 0 ]; then
   echo $answer | kty-merge $FORM_merges | while read temperature; do
     echo "<temperature>$temperature</temperature>"
   done
fi
echo "</response>"
?>
