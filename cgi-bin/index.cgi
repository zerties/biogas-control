#!/usr/bin/haserl
Content-Type: text/html

<html>
<head>
  <link href="/style.css"  media="screen" rel="Stylesheet" type="text/css" />
  <script src="/scripts.js" type="text/javascript"></script>
  <meta name="copyright" content="(C)opyright  2007 by Christian Dietrich <stettberger (at) dokucode.de">
  <title>Biogas Administration - Startseite</title>
</head>

<body>
<? cat header.html ?>
<div id='content'>
<h1>Biogas Administration</h1>
<p>Von diesem Punkt aus k&ouml;nnen sie verschiedene Aktionen in ihrer
Biogasgasanlage vornehmen, und sowohl den Status verschiedener Einzelsysteme, als
auch den Status des Gesamtsystems einsehen. Die Zust&auml;nde auf den
einzelnen Unterseiten aktualisieren sich regelm&auml;sig selbst. Sie
m&uuml;ssen die Seiten also nicht manuell aktualisieren.</p>

<noscript><b>Es wird auf jeden Fall Javascript ben&ouml;tigt</noscript>

<h3>Einzelne Module</h3>
<ul>
  <li>Schalter - Schalten von Pumpen/Lampen etc.</li>
  <li>Status - Einsehen des Netzwerkstatus</li>
</ul>

<p>Diese Software ist lizensiert unter der <a href="/COPYING.txt">GNU General
Public License</a></p>
</div>
</body>
</html>

