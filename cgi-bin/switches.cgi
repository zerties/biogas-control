#!/usr/bin/haserl
Content-Type: text/html

<? 
  config() {
    awk 'BEGIN{ a = 0 } {print a " " $0; a = a + 1;}' < ../config/switches 
  }
?>

<html>
<head>
  <title>Biogas Administration - Schalter</title>
  <meta name="copyright" content="(C)opyright  2007 by Christian Dietrich <stettberger (at) dokucode.de">
  <link href="/style.css"  media="screen" rel="Stylesheet" type="text/css" />
  <script src="/scripts.js" type="text/javascript"></script>
<script language="JavaScript"><!--

function get_status( id ) {
  var obj = returnObjById('status' + id);
  var req = nc_command(obj.getAttribute('address'), 'pin get ' +
             obj.getAttribute('getter'), get_status_handler);
  req.switch_id = id;
}

function get_status_handler(request) {
  var obj = returnObjById('status' + request.switch_id);
  if (!nc_error(request)) {
    var link = returnObjById('link' + request.switch_id);
    if (nc_get_text(request) == 'on') {
      obj.innerHTML = 'gestartet';
      obj.setAttribute("class", "statusrunning");
      link.innerHTML = "stoppen";
    } else {
      link.innerHTML = "starten";
      obj.innerHTML = 'gestopt';
      obj.setAttribute("class", "statusstopped");
    }
  }
}

function translate_status( status ) {
  if (status == 'on') {
    return 'gestartet';
  } else {
    return 'gestopt';
  }
}

function retranslate_status(translated) {
  if (translated == 'gestartet') {
    return 'on';
  } else {
    return 'off';
  }
}

function toggle_status( id ) {
  var obj = returnObjById('status' + id);
  
  // toggle status
  var status = retranslate_status(obj.innerHTML);
  if (status == 'on') {
    status = 'off';
  } else {
    status = 'on';
  }

  var req = nc_command(obj.getAttribute('address'), 
                       'pin set ' + obj.getAttribute('setter') + " " + status,
                       toggle_status_handler);
  req.switch_id = id;
}

function toggle_status_handler ( request ) {
  var obj = returnObjById('status' + request.switch_id);
  if (!nc_error(request)) {
    logger(0, obj.getAttribute('identifier') + " umgeschaltet ( Neuer Status: "
           + translate_status(nc_get_text(request)) + " )");
    get_status(request.switch_id);
  }
}

window.onload = function() {
<?
# Add all refresh handlers
config | awk '{print "get_status("$1"); setInterval(\"get_status("$1")\", 30000);\
  "}'
?>
}
//--></script>
</head>

<body>
<? cat header.html ?>
<div id='content'>
<table border="1" cellspacing="0" padding="4">
  <tr style='background-color: grey;'><td>Name</td><td>Status</td><td>Aktion</td></tr>
  <? config | awk '
  {print "<tr><td>" $2 "</td><td identifier=\"" $2 "\" address=\"" $3 "\" \
    getter=\"" $4 "\"  setter=\"" $5 "\" id=\"status" $1 "\">unbekannt</td> \
    <td><a href=\"javascript:toggle_status(" $1 ")\" id=link" $1 ">umschalten</a></tr>"}'
  ?>
</table>
</div>
<div id='logconsole'>
</div>
</body>
</html>

